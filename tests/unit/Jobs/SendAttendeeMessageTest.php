<?php

namespace Tests\Unit\Jobs;

use App\Concert;
use OrderFactory;
use Tests\TestCase;
use App\AttendeeMessage;
use App\Jobs\SendAttendeeMessage;
use App\Mail\AttendeeMessageEmail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class SendAttendeeMessageTest extends TestCase
{
    use DatabaseMigrations;

    /** @test */
    function it_sends_the_message_to_all_concert_attendees()
    {
        Mail::fake();

        $concert = factory(Concert::class)->create();
        $otherConcert = factory(Concert::class)->create();

        $message = AttendeeMessage::create([
            'concert_id' => $concert->id,
            'subject' => 'My subject',
            'message' => 'My message'
       ]);

        $orderA = OrderFactory::createForConcert($concert, ['email' => 'user1@gmail.com']);
        $otherOrder = OrderFactory::createForConcert($otherConcert, ['email' => 'user4@gmail.com']);
        $orderB = OrderFactory::createForConcert($concert, ['email' => 'user2@gmail.com']);
        $orderC = OrderFactory::createForConcert($concert, ['email' => 'user3@gmail.com']);

        SendAttendeeMessage::dispatch($message);

        Mail::assertQueued(AttendeeMessageEmail::class, function($mail) use ($message) {
            return $mail->hasTo('user1@gmail.com') 
                && $mail->attendeeMessage->is($message);
        });

        Mail::assertQueued(AttendeeMessageEmail::class, function($mail) use ($message) {
            return $mail->hasTo('user2@gmail.com')
                && $mail->attendeeMessage->is($message);
        });

        Mail::assertQueued(AttendeeMessageEmail::class, function($mail) use ($message) {
            return $mail->hasTo('user3@gmail.com')
                && $mail->attendeeMessage->is($message);
        });

        Mail::assertNotSent(AttendeeMessageEmail::class, function($mail) {
            return $mail->hasTo('user4@gmail.com');
        });
    }   
}