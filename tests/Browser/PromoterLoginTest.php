<?php

namespace Tests\Browser;

use App\User;
use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class PromoterLoginTest extends DuskTestCase
{
    use DatabaseMigrations;

    /** @test */
    public function logging_in_successfully()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret')
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('email', 'admin@gmail.com')
                ->type('password', 'secret')
                ->press('Log in')
                ->assertPathIs('/backstage/concerts');
        });
    }

    /** @test */
    public function logging_in_with_invalid_credentials_are_failed()
    {
        $user = factory(User::class)->create([
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret')
        ]);

        $this->browse(function (Browser $browser) {
            $browser->visit('/login')
                ->type('email', 'admin@gmail.com')
                ->type('password', 'wrong_password')
                ->press('Log in')
                ->assertPathIs('/login')
                ->assertInputValue('email', 'admin@gmail.com')
                ->assertSee('credentials do not match');
        });
    }
}
